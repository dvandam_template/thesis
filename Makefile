CC   = latexmk
NAME = report

.PHONY: all main view

all: main glossary

view:
	xdg-open $(NAME).pdf

main:
	$(CC) -pdf $(NAME).tex

glossary:
	makeglossaries $(NAME)

clean:
	$(CC) -C

cleanall: clean
	rm -f *.acn *.acr *.alg *.bbl *.glg *.glo *.gls *.ist $(NAME)-blx.bib \
	$(NAME).run.xml
